<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ncws46A5MkEL2HiKihlVdlGtAOxOsu4/dBHMs/k5XNsI3WbNP7VcBniZCJOdRxDri4NgYrb71O6gqV3JyXmn1g==');
define('SECURE_AUTH_KEY',  'ZXYACSsQ4+dLleqZlCPpDYp4dSSND26l+iNMJbkW1uBY6npgOY6puVlyZPJXMPLidyr+gOF+CkfEojUUsAyAwQ==');
define('LOGGED_IN_KEY',    'WghYD4kgQX2TrkR5U6O0T5E+XdH2exzaHVA97Piz9MLa3xOeb6BReTKmBui+nD3hiB4pEbFJDRCaEOIqzyzjFQ==');
define('NONCE_KEY',        'yGYdr4OXQf5xLkeGgclIBBYZZxBE0UBhKDS2oIqWLG7tcZVKK9j6oIdZPEtRrnmxz7vsBRJy7G3XsKysHU6pQA==');
define('AUTH_SALT',        'qnF39mWk0yjZ5mr3vtdLwuOf0os2PgoryO17FfvW9JrOlUK8U7UfWfFiGtxOEoU9MihkHePIlsBM9yshJNNEVw==');
define('SECURE_AUTH_SALT', 'tNw24uYNk/nro3mUtNGG5ReixeQmqpnVKzDg/gptgF9nxrnoSqW5o9B5ZUJOiWSqOfLJpk+Dcl+EeKPwpUb0+g==');
define('LOGGED_IN_SALT',   'ZiS+wWRkcOt0xN2WGJMIkTY0DOfuOuY/knRkYQ0Q6Y8wSfl75DnzPwMSh/Sse7P6cBVdnPKPRBwFJ1oa8kmugA==');
define('NONCE_SALT',       'io54aoWXuiaF4GCU1Aw6aZ/66IsJQYYXRPvDm5wUhXcyx9xKJ3d19gP2xRdYJudewM4LXTNww4jz7Eg1U9jwqg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
