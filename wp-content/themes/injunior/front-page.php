

<?php get_header()?>
    <main>
        <p>front-page</p>
        <h1><?php the_title() ?></h1>
        <p><?php the_field('paragrafo') ?></p>
        <p><?php the_field('texto') ?></p>
        <?php $image = (get_field('imagem')) ?>
        <img width="<?php echo $image['width'] ?>" height="<?php echo $image['height'] ?>" src="<?php echo $image['url'] ?>" alt="">
        <?php get_template_part( 'inc', 'section', array('key' => 'INCLUDE TITLE')) ?>
    </main>
<?php get_footer()?>
    