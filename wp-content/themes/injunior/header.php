<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php bloginfo('description')?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- começo wp_head -->
    <?php wp_head()?>
    <!-- fim wp_head -->
</head>
<body <?php body_class()?>>
    <!-- começo wp_body_open -->
    <?php wp_body_open()?>
    <!-- fim wp_body_open -->
    <header>
        <?php 
            $args = array(
                'menu' => 'navegacao',
                'container' => 'nav',
            );
            wp_nav_menu( $args ) 
        ?>
    </header>