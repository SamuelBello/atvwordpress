<?php get_header()?>
    <main>
        <p>single.php</p>
        <h1><?php the_title() ?></h1>
        <p><?php the_content() ?></p>
    </main>
<?php get_footer()?>