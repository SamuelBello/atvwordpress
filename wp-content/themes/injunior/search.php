<?php get_header()?>
    <main>
        <h1>Search.php</h1>
        <?php 
        get_search_form();
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <!-- conteúdo do post -->
            <h2><?php the_title() ?></h2>
            <p><?php the_content() ?></p>
            <a href="<?php the_permalink() ?>">Veja</a>
        <?php endwhile; else : ?>
	        <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; 
            echo paginate_links()
        ?>
    </main>
<?php get_footer()?>
    