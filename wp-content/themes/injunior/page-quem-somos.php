<?php 
    /*
        Template Name: Quem Somos
    */
?>
<?php get_header()?>
    <main>
        <p>page-quem-somos</p>
        <h1><?php the_title() ?></h1>
        <p><?php the_content() ?></p>
    </main>
<?php get_footer()?>
    