<?php 
    /*
        Template Name: Sobre
    */
?>
<?php get_header()?>
    <main>
        <p>sobre.php</p>
        <h1><?php the_title() ?></h1>
        <p><?php the_content() ?></p>
        </br>
        <?php get_template_part( 'inc', 'section', array('key' => 'INCLUDE TITLE')) ?>
    </main>
<?php get_footer()?>
    