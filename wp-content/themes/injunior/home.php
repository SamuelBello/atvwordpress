

<?php get_header()?>
    <main>
        <p>home.php</p>
        <h1>Posts</h1>
        <?php 
        get_search_form();
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <!-- conteúdo do post -->
            <h3><?php the_title() ?></h3>
            <a href="<?php the_permalink() ?>">Veja</a>
        <?php endwhile; else : ?>
	        <p><?php esc_html_e( 'Nenhum post encontrado.' ); ?></p>
        <?php endif; 
            echo paginate_links()
        ?>
        <h2>Custom Posts</h2>
        <?php 
            $args = array (
                'post_type'         =>  'noticia',
                'post_status'       =>  'publish',
                'suppress_filters'  =>  true,
                'orderby'           =>  'post_date',
                'order'             =>  'DESC'
            );

            $news = new WP_Query( $args );?>
            <?php
            if ( $news -> have_posts() ) : while ( $news -> have_posts() ) : $news -> the_post(); ?>
            <h3><a href="<?php the_permalink() ?>  "><?php the_title();?></a></h3>
            <?php endwhile; else : esc_html_e( 'Nenhum post encontrado.' );
            endif; ?> <br>
            
    </main>
<?php get_footer()?>
    