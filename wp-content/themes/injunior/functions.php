<?php 
    function samuel_scripts(){
        wp_enqueue_style( 'samuel-reset-style', get_template_directory_uri().'/css/reset.css');
        wp_enqueue_style( 'samuel-style', get_template_directory_uri().'/style.css');
    }
    add_action( 'wp_enqueue_scripts', 'samuel_scripts');   
    
    add_theme_support( 'menus' );
    add_theme_support( 'title-tag' );
    add_theme_support('post-thumbnails', array('noticias'));

    function custom_post_type_noticias() {
        register_post_type('noticia', array(
            'label' => 'Notícias',
            'description' => 'Descrição Notícias',
            'menu_position' =>  2,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'query_var' => true,
            'supports' => array('title', 'editor', 'thumbnail'),
     
            'labels' => array (
                'name' => 'Notícia',
                'singular_name' => 'Notícia',
                'menu_name' => 'Notícias',
                'add_new' => 'Nova Notícia',
                'add_new_item' => 'Adicionar Nova Notícia',
                'edit' => 'Editar Notícia',
                'edit_item' => 'Editar Notícia',
                'new_item' => 'Nova Notícia',
                'view' => 'Ver Notícia',
                'view_item' => 'Ver Notícia',
                'search_items' => 'Procurar Notícia',
                'not_found' => 'Nenhuma Notícia Encontrada',
                'not_found_in_trash' => 'Nenhuma Notícia Encontrada no Lixo',
            )
        )); 
    }add_action( 'init', 'custom_post_type_noticias');

?>